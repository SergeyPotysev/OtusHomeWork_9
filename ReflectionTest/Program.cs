﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;

namespace ReflectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Sample sample = new Sample()
            {
                I1 = 1,
                I2 = 2,
                I3 = 3,
                I4 = 4,
                I5 = 5
            };

            // Сериализация в файл
            string csv = MySerializer.ToCSV(sample, true);
            string json = Newtonsoft.ToJSON(sample, true);
            Console.WriteLine($"Сериализация:\nCSV  - {csv}\nJSON - {json}");
            // Десериализация из файла 
            Sample fromCsv = MySerializer.FromCSV<Sample>(string.Empty, true);
            Sample fromJson = Newtonsoft.FromJSON<Sample>(string.Empty, true);
            Console.WriteLine("\nДесериализация:\n\tCSV");
            MySerializer.PrintObject(fromCsv);
            Console.WriteLine("\tJSON");
            MySerializer.PrintObject(fromJson);

            // Количество итераций
            int n = 100000;

            List<long> csvTimes = new List<long>();
            List<long> jsonTimes = new List<long>();
            var sw = new Stopwatch();
            for (int i = 0; i < n; i++)
            {
                sw.Start();
                MySerializer.ToCSV(sample);
                MySerializer.FromCSV<Sample>(csv);
                csvTimes.Add(sw.ElapsedTicks);
                sw.Restart();
                Newtonsoft.ToJSON(sample);
                Newtonsoft.FromJSON<Sample>(json);
                jsonTimes.Add(sw.ElapsedTicks);
                sw.Reset();
            }
            Console.WriteLine($"\nКоличество итераций: {n}");

            Statistics(csvTimes, out long min, out long max, out double avg, out long sum);
            csvTimes.Clear();
            sw.Start();
            Console.WriteLine($"CSV  - Min: {min}, Max: {max}, Average: {avg}, Sum: {sum} ticks ({(new TimeSpan(sum)).TotalMilliseconds} ms)");
            Console.WriteLine($"Время на вывод текста в консоль (ticks): {sw.ElapsedTicks}");
            sw.Reset();

            Statistics(jsonTimes, out min, out max, out avg, out sum);
            jsonTimes.Clear();
            sw.Start();
            Console.WriteLine($"JSON - Min: {min}, Max: {max}, Average: {avg}, Sum: {sum} ticks ({(new TimeSpan(sum)).TotalMilliseconds} ms)");
            Console.WriteLine($"Время на вывод текста в консоль (ticks): {sw.ElapsedTicks}");
            sw.Reset();

            Console.ReadKey();
        }


        private static void Statistics(List<long> times, out long min, out long max, out double avg, out long sum)
        {
            min = times.Min();
            max = times.Max();
            avg = times.Average();
            sum = times.Sum();
        }

    }
}

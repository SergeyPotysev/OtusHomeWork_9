﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ReflectionTest
{
    public class MySerializer
    {
        /// <summary>
        /// Сериализация полей и свойств объекта в формат CSV с сохранением в файл.
        /// </summary>
        /// <typeparam name="T">Тип сериализуемого объекта.</typeparam>
        /// <param name="obj">Объект сериализации.</param>
        /// <param name="toFile">Признак необходимости записи в файл.</param>
        /// <param name="fileName">Имя файла для сохранения сериализованного объекта.</param>
        public static string ToCSV<T>(T obj, bool toFile = false, string fileName = "Test.csv")
        {
            string csv = string.Empty;
            var type = typeof(T);
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            // Обработка списка полей типа T
            foreach (var field in fields)
            {
                if (csv.Length > 0)
                {
                    csv += ",";
                }
                var fieldInfo = type.GetField(field.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                csv += fieldInfo.GetValue(obj); 
            }
            // Обработка списка свойств типа T
            var props = type.GetProperties();
            foreach (var prop in props)
            {
                if (csv.Length > 0)
                {
                    csv += ",";
                }
                var prorInfo = type.GetProperty(prop.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                csv += prorInfo.GetValue(obj);
            }
            if(toFile)
            {
                File.WriteAllText(fileName, csv);
            }
            return csv;
        }


        /// <summary>
        /// Создание объекта на основе десериализации CSV-строки.
        /// </summary>
        /// <typeparam name="T">Тип десериализуемого объекта.</typeparam>
        /// <param name="csv">Строка в формате CSV.</param>
        /// <param name="fromFile">Признак необходимости чтения из файла.</param>
        /// <param name="fileName">Имя файла для чтения сериализованного объекта.</param>
        /// <returns>Десериализованный объект.</returns>
        public static T FromCSV<T>(string csv, bool fromFile = false, string fileName = "Test.csv") where T : new()
        {
            T obj = new T();
            int i = 0;
            try
            {
                if(fromFile)
                {
                    csv = File.ReadAllText(fileName);
                }
                string[] members = csv.Split(',');                
                var type = typeof(T);
                var fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                // Обработка списка полей типа T
                foreach (var field in fields)
                {
                    if (i >= members.Count())
                        throw new NotImplementedException("CSV-файл не соответствует типу объекта!");
                    var fieldInfo = type.GetField(field.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    fieldInfo.SetValue(obj, Convert.ChangeType(members[i++], field.FieldType));
                }
                // Обработка списка свойств типа T
                var props = type.GetProperties();
                foreach (var prop in props)
                {
                    if (i >= members.Count())
                        throw new NotImplementedException("CSV-файл не соответствует типу объекта!");
                    var prorInfo = type.GetProperty(prop.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    prorInfo.SetValue(obj, Convert.ChangeType(members[i++], prop.PropertyType));
                }
            }
            catch (NotImplementedException notImp)
            {
                Console.WriteLine(notImp.Message);
                obj = new T();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                obj = new T();
            }
            return obj;
         }


        public static void PrintObject<T>(T obj)
        {
            var type = typeof(T);
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var field in fields)
            {                
                var fieldInfo = type.GetField(field.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                Console.WriteLine($"{field.Name}: {fieldInfo.GetValue(obj)}");
            }
        }

    }
}

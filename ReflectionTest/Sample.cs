﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReflectionTest
{
    [Serializable]
    public class Sample
    {
        public int I1, I2, I3, I4, I5;
        [JsonProperty]
        private int _p;
        public Sample()
        { 
        }
    }
}

﻿using Newtonsoft.Json;
using System.IO;

namespace ReflectionTest
{
    class Newtonsoft
    {
        /// <summary>
        /// Сериализация объекта в формат JSON с сохранением в файл.
        /// </summary>
        /// <typeparam name="T">Тип сериализуемого объекта.</typeparam>
        /// <param name="obj">Объект сериализации.</param>
        /// <param name="toFile">Признак необходимости сохранения в файл.</param>
        /// <param name="fileName">Имя файла для сохранения сериализованного объекта.</param>
        /// <returns></returns>
        public static string ToJSON<T>(T obj, bool toFile = false, string fileName = "Test.json")
        {
            var json = JsonConvert.SerializeObject(obj);
            if(toFile)
            {
                File.WriteAllText(fileName, json);
            }
            return json;
        }


        /// <summary>
        /// Создание объекта на основе десериализации JSON-строки.
        /// </summary>
        /// <typeparam name="T">Тип десериализуемого объекта.</typeparam>
        /// <param name="json">Строка в формате JSON.</param>
        /// <param name="fromFile">Признак необходимости чтения из файла.</param>
        /// <param name="fileName">Имя файла для чтения сериализованного объекта.</param>  
        /// <returns>Десериализованный объект.</returns>
        public static T FromJSON<T>(string json, bool fromFile = false, string fileName = "Test.json") 
        {
            if(fromFile)
            {
                json = File.ReadAllText(fileName);
            }  
            T obj = JsonConvert.DeserializeObject<T>(json);
            return obj;
        }
    }
}